public class ZSemanticException extends RuntimeException{
    public ZSemanticException(String message) {
        super(message);
    }
}
