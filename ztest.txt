Algo

Text globalVariable = "global String";
function int myFunc(Text var){
    int x = 5;
    int result= 0;
    x = 3;
    if (x < 0){
        x=0;
    }else{
        result = 1;
        while(x>0){

            result = result * x;
            x = x - 1;
        }
        print(result);
    }
    return result;
}

proc myProcedure(int a, int b){

	print( a+b );
}

main{
    print("Hello world");

    int finalResult = myFunc("string");
    print("Final result is = "+finalResult);
    myProcedure(1,5);
}
