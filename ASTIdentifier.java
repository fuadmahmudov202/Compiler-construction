/* Generated By:JJTree: Do not edit this line. ASTIdentifier.java Version 7.0 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
public
class ASTIdentifier extends SimpleNode {
  public ASTIdentifier(int id) {
    super(id);
  }

  public ASTIdentifier(AlgoCalc p, int id) {
    super(p, id);
  }
  private String value;


  public void setValue(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }



  /** Accept the visitor. **/
  public Object jjtAccept(AlgoCalcVisitor visitor, Object data) {

    return
    visitor.visit(this, data);
  }
}
/* JavaCC - OriginalChecksum=d0116a45cdf1ca73d5e319e2e61d8171 (do not edit this line) */
