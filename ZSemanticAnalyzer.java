import java.util.*;

public class ZSemanticAnalyzer implements AlgoCalcVisitor {
    private final ZSymbolTable symbolTable = new ZSymbolTable();

    @Override
    public Object visit(SimpleNode node, Object data) {
        System.out.println("visit is working" );
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTStart node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }


    public Object visit(ASTProgram node, Object data) {
        symbolTable.enterScope(); // Start of program scope
        node.childrenAccept(this, data);
        symbolTable.exitScope(); // End of program scope
        return null;
    }


    @Override
    public Object visit(ASTGlobalDeclarations node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTVariableDeclaration node, Object data) {
        ASTVariableDeclaration variableDeclaration = (ASTVariableDeclaration) node.value;
        String name = variableDeclaration.getName();
        String type = variableDeclaration.getType();

        // Check if the type is null
        if (type == null) {
            System.out.println("Semantic Error: Variable " + name + " has no type specified.");
            return null;
        }

        // Check if the initialization expression exists
        if (node.jjtGetNumChildren() > 0) {
            String expressionType=null;
            if (node.jjtGetChild(0).getClass().getName().equals("ASTStringLiteral")){
                expressionType="TEXT";
            }
            else if (node.jjtGetChild(0).getClass().getName().equals("ASTNumber")) {
                expressionType="int";
            }else {
                expressionType="int";

            }

            // Check if the declared type matches the type of the initialization expression
            if (!type.equalsIgnoreCase(expressionType)) {
                System.out.println("Semantic Error: Variable " + name + " is declared as " + type +
                        " but initialized with expression of type " + expressionType);
            }
        }

        // Declare the variable in the symbol table
        if (!symbolTable.declareVariable(name, type)) {
            System.out.println("Semantic Error: Variable " + name + " is already declared in this scope.");
        }

        return null;
    }





    @Override
    public Object visit(ASTFunctionDeclaration node, Object data) {
        symbolTable.enterScope(); // Enter function scope
        // Process function declaration...
        String functionName = ((ASTIdentifier) node.jjtGetChild(0)).getValue(); // Assuming the function name is the first child
        String returnType = (String) node.jjtGetValue(); // Assuming the return type is stored as value in the node
        // Declare function in the symbol table
        if (!symbolTable.declareFunction(functionName, returnType)) {
            System.out.println("Semantic Error: Function " + functionName + " is already declared.");
        }

        // Process parameters
        ASTParameterList parameterList = (ASTParameterList) node.jjtGetChild(1); // Assuming parameter list is the second child
        parameterList.jjtAccept(this, data); // Process parameters

        // Process function body if it exists
        if (node.jjtGetNumChildren() > 2) {
            ASTFunctionBody functionBody = (ASTFunctionBody) node.jjtGetChild(2); // Assuming function body is the third child
            functionBody.jjtAccept(this, data);
        }

        symbolTable.exitScope(); // Exit function scope
        return null;
    }






    @Override
    public Object visit(ASTProcedureDeclaration node, Object data) {
        symbolTable.enterScope(); // Enter procedure scope

        // Process procedure declaration...
        String procedureName = ((ASTIdentifier) node.jjtGetChild(0)).getValue(); // Assuming the procedure name is the first child

        // Declare procedure in the symbol table
        if (!symbolTable.declareProcedure(procedureName)) {
            System.out.println("Semantic Error: Procedure " + procedureName + " is already declared.");
        }

        // Process parameter list if any
        if (node.jjtGetNumChildren() > 1) {
            ASTParameterList parameterList = (ASTParameterList) node.jjtGetChild(1); // Assuming parameter list is the second child
            parameterList.jjtAccept(this, data);
        }


        symbolTable.exitScope(); // Exit procedure scope
        return null;
    }



    @Override
    public Object visit(ASTMainBlock node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTParameterList node, Object data) {
        // Iterate through parameter list and declare each parameter as a variable within the function's scope
        for (int i = 0; i < node.jjtGetNumChildren(); i += 2) {
            ASTTypeList typeList = (ASTTypeList) node.jjtGetChild(i);
            ASTIdentifier identifier = (ASTIdentifier) node.jjtGetChild(i + 1);
            String parameterName = identifier.getValue();
            String parameterType = (String) typeList.jjtGetValue(); // Assuming TypeList node stores the type as value
            // Declare parameter as a variable within the function's scope
            if (!symbolTable.declareVariable(parameterName, parameterType)) {
                System.out.println("Semantic Error: Parameter " + parameterName + " is already declared.");
            }
        }
        return null;
    }

    @Override
    public Object visit(ASTTypeList node, Object data) {
        return node.jjtGetValue();
    }


    @Override
    public Object visit(ASTFunctionBody node, Object data) {
        symbolTable.enterScope();
        node.childrenAccept(this, data);
        symbolTable.exitScope();
        return null;
    }

    @Override
    public Object visit(ASTProcedureBody node, Object data) {
        symbolTable.enterScope();
        node.childrenAccept(this, data);
        symbolTable.exitScope();
        return null;
    }

    @Override
    public Object visit(ASTMainBody node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTStatements node, Object data) {
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            SimpleNode statement = (SimpleNode) node.jjtGetChild(i);
            statement.jjtAccept(this, data); // Visit the statement node
        }
        return null;
    }

    @Override
    public Object visit(ASTStatement node, Object data) {
        node.childrenAccept(this, data);
        return null;
    }

    @Override
    public Object visit(ASTAssignment node, Object data) {
        node.childrenAccept(this, data); // Proceed to visit children
        return null;
    }




    @Override
    public Object visit(ASTIfStatement node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTWhileLoop node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, data);
        node.jjtGetChild(1).jjtAccept(this, data);
        if (node.jjtGetNumChildren() == 3) {
            node.jjtGetChild(2).jjtAccept(this, data);
        }
        return null;
    }

    @Override
    public Object visit(ASTPrintStatement node, Object data) {
        node.jjtGetChild(0).jjtAccept(this, null);

        return null;
    }

    @Override
    public Object visit(ASTCall node, Object data) {
        String functionName = findFunctionName(node);
        if (symbolTable.lookupFunction(functionName) == null && !symbolTable.lookupProcedure(functionName)) {
            System.out.println("Semantic Error: Function or procedure " + functionName + " is not declared.");
        }
        node.childrenAccept(this, data);

        return null;
    }

    private String findFunctionName(SimpleNode node) {
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            SimpleNode child = (SimpleNode) node.jjtGetChild(i);
            if (child instanceof ASTIdentifier) {
                return ((ASTIdentifier) child).getValue();
            }
        }
        return null;
    }
    @Override
    public Object visit(ASTExpression node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTSimpleExpression node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTTerm node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTFactor node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    // Implement other visit methods as necessary...

    // Override visit methods for nodes where semantic checks are needed
    // Example for an identifier use in an expression:

    @Override
    public Object visit(ASTStringLiteral node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    @Override
    public Object visit(ASTNumber node, Object data) {
        node.childrenAccept(this, data);

        return null;
    }

    // Example for a function call:
    @Override
    public Object visit(ASTFunctionCall node, Object data) {
        String functionName = node.getFunctionName();
        if (symbolTable.lookupFunction(functionName) == null && !symbolTable.lookupProcedure(functionName)) {
            System.out.println("Semantic Error: Function or procedure " + functionName + " is not declared.");
        }
        return null;
    }



    @Override
    public Object visit(ASTArguments node, Object data) {
        node.childrenAccept(this, data);


        return null;
    }

    @Override
    public Object visit(ASTIdentifier node, Object data) {
        String name = node.getValue();
        if (symbolTable.lookupVariable(name) == null && !symbolTable.lookupProcedure(name) ) {
            System.out.println("Semantic Error: Variable " + name + " is not declared.");
        }
        return null;
    }



    // Add necessary utility methods and visit methods for other node types
}
