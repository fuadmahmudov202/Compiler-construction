import java.util.Arrays;

public class ASTVariableDeclaration extends SimpleNode {
  private String name;
  private String type;


  public ASTVariableDeclaration(int id) {
    super(id);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public Object jjtAccept(AlgoCalcVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }
}
