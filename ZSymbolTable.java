import java.util.*;

class ZSymbolTable {
    private final Deque<Map<String, String>> scopes = new LinkedList<>();
    private final Map<String, String> functions = new HashMap<>();
    private final Set<String> procedures = new HashSet<>();

    public void enterScope() {
        scopes.push(new HashMap<>());
    }

    public void exitScope() {
        System.out.println("exit scope "+scopes.toString());
        scopes.pop();
    }

    public boolean declareVariable(String name, String type) {
        Map<String, String> currentScope = scopes.peek();
        if (currentScope.containsKey(name)) {
            System.out.println();
            return false; // Variable already declared in this scope
        }
        currentScope.put(name, type);
        return true;
    }

    public String lookupVariable(String name) {
        for (Map<String, String> scope : scopes) {
            if (scope.containsKey(name)) {
                return scope.get(name);
            }
        }
        return null; // Variable not found
    }

    public boolean declareFunction(String name, String returnType) {
        if (functions.containsKey(name)) {
            return false; // Function already declared
        }
        functions.put(name, returnType);
        return true;
    }
    public String lookupFunctionReturnType(String name) {
        return functions.get(name);
    }
    public String lookupFunction(String name) {
        return functions.get(name);
    }

    public boolean lookupProcedure(String name) {
        return procedures.contains(name);
    }

    public boolean declareProcedure(String name) {
        if (functions.containsKey(name) || procedures.contains(name)) {
            return false; // Function or procedure already declared
        }
        procedures.add(name);
        return true;
    }
}
